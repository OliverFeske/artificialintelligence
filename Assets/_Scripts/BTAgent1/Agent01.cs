﻿using System.Collections.Generic;
using UnityEngine;
using BehaviourTree;

public class Agent01 : MonoBehaviour, IMoveable
{
	[SerializeField] private float moveSpeed;
	[SerializeField] private Transform targetTransform;

	[SerializeField] private AgentData data;
	private Context<AgentData> context;
	private Root root;
	private Status treeStatus = Status.Running;

	#region MonoBehaviour
	void Start()
	{
		context = new Context<AgentData>(data);
		context.data.Rb = GetComponent<Rigidbody>();
		context.data.Debug = GetComponentInChildren<TextMesh>();
		context.data.CurTargetPos = targetTransform.position;
		context.data.Moveable = this;
		WaitTask wait = new WaitTask(2f);
		//PhysicsJumpTask jump = new PhysicsJumpTask(15f);
		MoveTask move = new MoveTask(10f);
		Sequence sequence = new Sequence(new List<Task>() { wait,/* jump, */move });
		Repeater repeater = new Repeater(sequence, 4);
		root = new Root(repeater);
	}
	void Update()
	{
		if (treeStatus != Status.Running)
			return;

		treeStatus = root.Update(context);
	}
	#endregion

	public void MoveTo(Vector3 targetPos, out bool hasArrived)
	{
		transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * moveSpeed);
		hasArrived = (transform.position - targetPos).sqrMagnitude < 0.01f ? true : false;
	}
}
[System.Serializable]
public class AgentData
{
	public TextMesh Debug;
	public Rigidbody Rb;
	public IMoveable Moveable;
	public Vector3 CurTargetPos;
	public int Level;
	public string Name;
}