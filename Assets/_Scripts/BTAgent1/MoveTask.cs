﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviourTree
{
	public class MoveTask : Task
	{
		private float maxTime;
		private float timer;

		public MoveTask(float _maxTime)
		{
			maxTime = _maxTime;
		}

		public override Status Update(Context context)
		{
			Context<AgentData> c = (Context<AgentData>)context;
			c.data.Moveable.MoveTo(c.data.CurTargetPos, out bool hasArrived);

			if (timer < maxTime)
				return hasArrived ? Status.Success : Status.Running;
			else
				return Status.Failure;
		}
	}
}