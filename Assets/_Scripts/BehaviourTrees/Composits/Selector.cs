﻿using System.Collections.Generic;

namespace BehaviourTree
{
	public class Selector : Composite
	{
		private int curChildIdx;

		public Selector(List<Task> childrenTasks) : base(childrenTasks)
		{
		}

		public override Status Update(Context context)
		{
			Context<AgentData> c = (Context<AgentData>)context;
			while (curChildIdx < childrenTasks.Count)
			{
				switch (childrenTasks[curChildIdx].Update(context))
				{
					case Status.Success:
						c.data.Debug.text += $"selector : success\n";
						curChildIdx = 0;
						return Status.Success;

					case Status.Failure:
						if (++curChildIdx < childrenTasks.Count)
						{
							c.data.Debug.text += $"selector : try next\n";
							continue;
						}
						else
						{
							c.data.Debug.text += $"selector : fail\n";
							curChildIdx = 0;
							return Status.Failure;
						}

					case Status.Running:
						c.data.Debug.text += $"sequence : running\n";
						return Status.Running;

					default:
						throw new System.Exception("sequence : unexpected child status return");
				}
			}
			throw new System.Exception("sequence : unexpected termination out of loop");
		}
	}
}
