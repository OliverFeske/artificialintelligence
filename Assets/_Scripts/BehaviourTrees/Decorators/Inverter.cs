﻿namespace BehaviourTree
{
	public class Inverter : Decorator
	{
		public Inverter(Task _childTask) : base(_childTask)
		{
		}

		public override Status Update(Context context)
		{
			switch (childTask.Update(context))
			{
				case Status.Success:
					return Status.Failure;

				case Status.Failure:
					return Status.Success;

				case Status.Running:
					return Status.Running;

				default:
					throw new System.Exception("inverter : unexpected status");
			}
		}
	}
}
