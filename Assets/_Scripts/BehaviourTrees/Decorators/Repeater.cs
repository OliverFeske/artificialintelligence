﻿namespace BehaviourTree
{
	public class Repeater : Decorator
	{
		private int times;
		private int counter;

		public Repeater(Task _childTask, int _times) : base(_childTask)
		{
			times = _times;
		}

		public override Status Update(Context context)
		{
			Context<AgentData> c = (Context<AgentData>)context;
			while (counter < times)
			{
				switch (childTask.Update(context))
				{
					case Status.Success:
						if (++counter < times)
						{
							c.data.Debug.text += $"repeater : childTask\n";
							continue;
						}
						else
						{
							c.data.Debug.text += $"repeater : success\n";
							counter= 0;
							return Status.Success;
						}

					case Status.Failure:
						if (++counter < times)
						{
							c.data.Debug.text += $"repeater : childTask\n";
							continue;
						}
						else
						{
							c.data.Debug.text += $"repeater : failure\n";
							counter = 0;
							return Status.Failure;
						}

					case Status.Running:
						c.data.Debug.text += $"repeater : running\n";
						return Status.Running;

					default:
						throw new System.Exception("repeater : unexpected child status return");
				}
			}
			throw new System.Exception("repeater : unexpected termination out of loop");
		}
	}
}
