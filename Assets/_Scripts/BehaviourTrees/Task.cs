﻿using System.Collections.Generic;
using UnityEngine;

namespace BehaviourTree
{
	public enum Status { Success, Failure, Running }

	public class Context
	{
	}
	public class Context<T> : Context
	{
		public T data;
		public Context(T _data)
		{
			data = _data;
		}
	}

	public abstract class Task
	{
		public abstract Status Update(Context context);
	}

	public abstract class Composite : Task
	{
		protected List<Task> childrenTasks;

		public Composite(List<Task> _childrenTasks)
		{
			childrenTasks = _childrenTasks;
		}
	}

	public abstract class Decorator : Task
	{
		protected Task childTask;

		public Decorator(Task _childTask)
		{
			childTask = _childTask;
		}
	}

	public interface IMoveable
	{
		void MoveTo(Vector3 targetPos, out bool hasArrived);
	}
}
