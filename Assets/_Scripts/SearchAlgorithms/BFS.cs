﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BFS : MonoBehaviour
{
	[SerializeField] private NavNode startNode;
	[SerializeField] private NavNode endNode;
	[SerializeField] private Color directionArrowColor;
	[SerializeField] private Color pathArrowColor;
	private NavGraph navGraph;
	private Dictionary<int, int> cameFrom = new Dictionary<int, int>();
	private List<NavNode> curRoute = new List<NavNode>();

	#region Scene UI
	void OnDrawGizmos()
	{
		if (navGraph == null) { return; }
		// draw directions to the 
		foreach (var keyValuePair in cameFrom)
		{
			if (keyValuePair.Value == -1) { continue; }

			Vector3 posFrom = navGraph.AllNodes[keyValuePair.Key].transform.position;
			Vector3 posTo = navGraph.AllNodes[keyValuePair.Value].transform.position;

			Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);

			Handles.color = directionArrowColor;
			Handles.ArrowHandleCap(1, posFrom, rot, (posTo - posFrom).magnitude, EventType.Repaint);
		}

		// draw a path to the goal
		for (int i = 0; i < curRoute.Count - 1; i++)
		{
			Vector3 posFrom = curRoute[i].transform.position;
			Vector3 posTo = curRoute[i + 1].transform.position;

			Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);

			Handles.color = pathArrowColor;
			Handles.ArrowHandleCap(1, posFrom, rot, (posTo - posFrom).magnitude, EventType.Repaint);
		}
	}
	#endregion

	#region Graph Evaluation
	public void ExploreGraph()
	{
		navGraph = GetComponent<NavGraph>();
		Queue<int> frontier = new Queue<int>();
		cameFrom.Clear();
		frontier.Enqueue(startNode.Idx);
		cameFrom.Add(startNode.Idx, -1);

		int curNodeIdx = -1;
		while (frontier.Count > 0)
		{
			curNodeIdx = frontier.Dequeue();
			List<Edge> curEdges = navGraph.AllNodes[curNodeIdx].Edges;

			for (int i = 0; i < curEdges.Count; i++)
			{
				if (!cameFrom.ContainsKey(curEdges[i].ToNode.Idx))
				{
					frontier.Enqueue(curEdges[i].ToNode.Idx);
					cameFrom.Add(curEdges[i].ToNode.Idx, curNodeIdx);
				}
			}
		}

		foreach (var keyValuePair in cameFrom)
		{
			Debug.Log($"Key: {keyValuePair.Key} | Points to Key: {keyValuePair.Value}");
		}
	}
	public List<NavNode> GetPath()
	{
		navGraph = GetComponent<NavGraph>();
		Queue<int> frontier = new Queue<int>();
		cameFrom.Clear();
		frontier.Enqueue(startNode.Idx);
		cameFrom.Add(startNode.Idx, -1);

		int curNodeIdx = -1;
		while (frontier.Count > 0)
		{
			curNodeIdx = frontier.Dequeue();

			if (curNodeIdx == endNode.Idx) { break; }

			List<Edge> curEdges = navGraph.AllNodes[curNodeIdx].Edges;
			for (int i = 0; i < curEdges.Count; i++)
			{
				if (!cameFrom.ContainsKey(curEdges[i].ToNode.Idx))
				{
					frontier.Enqueue(curEdges[i].ToNode.Idx);
					cameFrom.Add(curEdges[i].ToNode.Idx, curNodeIdx);
				}
			}
		}
		if (curNodeIdx != endNode.Idx) { return null; }
		List<NavNode> route = new List<NavNode>();

		while (curNodeIdx != startNode.Idx)
		{
			route.Add(navGraph.AllNodes[curNodeIdx]);
			curNodeIdx = cameFrom[curNodeIdx];
		}

		route.Add(startNode);
		route.Reverse();
		curRoute = route;

		return route;
	}
	#endregion
}

[CustomEditor(typeof(BFS))]
public class BFSEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		BFS bfs = (BFS)target;

		if (GUILayout.Button("Explore Graph"))
			bfs.ExploreGraph();

		if (GUILayout.Button("Get Path"))
			bfs.GetPath();
	}
}