﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DFS : MonoBehaviour
{
	[SerializeField] private NavNode startNode;
	[SerializeField] private NavNode goalNode;
	private NavGraph navGraph;
	private bool[] visited;

	public List<NavNode> FindPath()
	{
		navGraph = GetComponent<NavGraph>();
		Dictionary<NavNode, NavNode> parentDict = new Dictionary<NavNode, NavNode>();
		visited = new bool[navGraph.GetNodeCount()];
		visited[startNode.Idx] = true;

		Stack<Edge> edgeStack = new Stack<Edge>();
		for (int i = 0; i < startNode.Edges.Count; i++)
		{
			edgeStack.Push(startNode.Edges[i]);
		}

		while (edgeStack.Count > 0)
		{
			Edge next = edgeStack.Pop();

			if (visited[next.ToNode.Idx]) { continue; }

			// arrived
			if (next.ToNode.Idx == goalNode.Idx)
			{
				parentDict[next.ToNode] = next.FromNode;
				var currentNode = goalNode;
				List<NavNode> route = new List<NavNode>();

				while (currentNode != startNode)
				{
					route.Add(currentNode);
					currentNode = parentDict[currentNode];
				}

				route.Add(startNode);
				route.Reverse();

				foreach (NavNode node in route)
				{
					Debug.Log(node.Idx);
				}
			}

			visited[next.ToNode.Idx] = true;

			for (int j = 0; j < next.ToNode.Edges.Count; j++)
			{
				edgeStack.Push(next.ToNode.Edges[j]);
			}

			parentDict[next.ToNode] = next.FromNode;
		}

		return null;
	}
}

[CustomEditor(typeof(DFS))]
public class DFSEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		DFS dfs = (DFS)target;

		if (GUILayout.Button("Get Path"))
			dfs.FindPath();
	}
}