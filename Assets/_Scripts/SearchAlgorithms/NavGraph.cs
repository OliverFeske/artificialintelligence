﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NavGraph : MonoBehaviour
{
	public enum EditStates { MoveNode, DuplicateNode, ConnectNode, RemoveConnection, RemoveNode }
	public EditStates currentEditState = EditStates.MoveNode;
	[SerializeField] private NavGraphVisualizer visualizer = new NavGraphVisualizer();
	public float HandleSize = 1f;
	public float ButtonSize = 1f;
	public Color MoveNodeColor;
	public Color DuplicateNodeColor;
	public Color ConnectNodeColor;
	public Color RemoveConnectionColor;
	public Color RemoveNodeColor;
	public float buttonOffsetScale;
	private NavNode[] allNodes;
	public NavNode[] AllNodes => allNodes;

	public int GetNodeCount() => transform.childCount;

	public void ConstructGraph()
	{
		int numNodes = transform.childCount;
		allNodes = new NavNode[numNodes];
		Debug.Log("constructing graph!");

		for (int i = 0; i < numNodes; i++)
		{
			var navNode = transform.GetChild(i).GetComponent<NavNode>();
			navNode.Idx = i;
			navNode.name = i.ToString();
			allNodes[i] = navNode;
		}
	}

	void OnDrawGizmos()
	{
		visualizer.DrawGraph(transform);
	}
}

[CustomEditor(typeof(NavGraph))]
public class NavGraphEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		NavGraph navGraph = (NavGraph)target;

		if (GUILayout.Button("Construct NavGraph"))
			navGraph.ConstructGraph();
	}

	void OnSceneGUI()
	{
		NavGraph navGraph = (NavGraph)target;
		List<NavNode> nodes = new List<NavNode>(navGraph.AllNodes);

		DrawEditStateButtons(navGraph);

		EditNodes(navGraph, nodes);

		if (navGraph.currentEditState != NavGraph.EditStates.MoveNode)
			CheckForClick(navGraph, nodes);
	}

	void DrawEditStateButtons(NavGraph navGraph)
	{
		Handles.BeginGUI();

		if (GUI.Button(new Rect(20f, 20f, 100f, 20f), "Move Node"))
		{
			navGraph.currentEditState = NavGraph.EditStates.MoveNode;
		}
		else if (GUI.Button(new Rect(140f, 20f, 100f, 20f), "Duplicate Node"))
		{
			navGraph.currentEditState = NavGraph.EditStates.DuplicateNode;
		}
		else if (GUI.Button(new Rect(260f, 20f, 100f, 20f), "Connect Node"))
		{
			navGraph.currentEditState = NavGraph.EditStates.ConnectNode;
		}
		else if (GUI.Button(new Rect(380f, 20f, 125f, 20f), "Remove Connection"))
		{
			navGraph.currentEditState = NavGraph.EditStates.RemoveConnection;
		}
		else if (GUI.Button(new Rect(525f, 20f, 100f, 20f), "Remove Node"))
		{
			navGraph.currentEditState = NavGraph.EditStates.RemoveNode;
		}

		Handles.EndGUI();
	}

	void EditNodes(NavGraph navGraph, List<NavNode> nodes)
	{

		EditorGUI.BeginChangeCheck();

		// draw a handle to every node
		Vector3[] newTargetPositions = new Vector3[nodes.Count];
		for (int i = 0; i < nodes.Count; i++)
		{
			Handles.color = navGraph.MoveNodeColor;
			newTargetPositions[i] = Handles.FreeMoveHandle(
				nodes[i].transform.position,
				Quaternion.identity,
				navGraph.HandleSize,
				Vector3.one,
				Handles.DotHandleCap);
		}

		if (EditorGUI.EndChangeCheck())
		{
			for (int i = 0; i < nodes.Count; i++)
			{
				nodes[i].transform.position = newTargetPositions[i];
			}
		}
	}

	void CheckForClick(NavGraph navGraph, List<NavNode> nodes)
	{
		for (int i = 0; i < nodes.Count; i++)
		{
			Quaternion rot = Quaternion.LookRotation(Camera.current.transform.forward, Camera.current.transform.up);
			Vector3 pos = nodes[i].transform.position + rot * (Vector3.up * navGraph.buttonOffsetScale);
			Handles.color = Color.white;
			if (Handles.Button(pos, rot, navGraph.ButtonSize, navGraph.ButtonSize, Handles.DotHandleCap))
			{
				switch (navGraph.currentEditState)
				{
					case NavGraph.EditStates.MoveNode:
						Debug.Log("do nothing");
						break;
					case NavGraph.EditStates.DuplicateNode:
						// click to duplicate
						// directly switch to movestate
						GameObject.Instantiate(nodes[i], nodes[i].transform.position + new Vector3(0f, 0f, 5f), nodes[i].transform.rotation, navGraph.transform);
						navGraph.ConstructGraph();
						Debug.Log("duplicate node!");
						break;
					case NavGraph.EditStates.ConnectNode:
						// click to link node to another one
						// click on the other node to connect it
						Debug.Log("connect node!");
						break;
					case NavGraph.EditStates.RemoveNode:
						// click to remove Node -> just recalculate the path and remove nulls in the edge lists of all nodes
						// better to just find a way of removing edges from adjacent nodes, which go to the current Node, but cant be accessed
						// by our current node -> maybe link them somehow
						Debug.Log("remove node!");
						break;
					default:
						break;
				}
			}
		}
	}
}

[System.Serializable]
public class NavGraphVisualizer
{
	[SerializeField] private bool doVisualize = false;
	[SerializeField] private float sphereRadius = 1f;
	[SerializeField] private float edgeOffset = 0.2f;
	[SerializeField] private float dirCubeScale = 0.1f;
	[SerializeField] private float dirCubeOffset = 0.2f;
	[SerializeField] private float costOffset = 1f;
	[SerializeField] private int idxFontSize = 28;
	[SerializeField] private int costFontSize = 12;

	[Header("Colors")]
	[SerializeField] private Color nodeSphereColor;
	[SerializeField] private Color nodeDistColor;
	[SerializeField] private Color toNodePosColor;
	[SerializeField] private Color nodeDirColor;

	public void DrawGraph(Transform graphTransform)
	{
		if (!doVisualize) { return; }

		foreach (Transform child in graphTransform)
		{
			Gizmos.color = nodeSphereColor;
			Gizmos.DrawWireSphere(child.position, sphereRadius);
			GUIStyle style_Idx = new GUIStyle();
			style_Idx.fontSize = idxFontSize;
			GUIStyle style_cost = new GUIStyle();
			style_cost.fontSize = costFontSize;
			style_cost.normal.textColor = nodeDistColor;
			var navNode = child.GetComponent<NavNode>();
			Handles.Label(child.position, navNode.Idx.ToString(), style_Idx);
			foreach (Edge edge in navNode.Edges)
			{
				if (edge.ToNode == null) { continue; }
				Gizmos.color = toNodePosColor;
				Vector3 toNodePos = edge.ToNode.transform.position;
				Vector3 dir = (toNodePos - child.position).normalized;
				Vector3 offset = Vector3.Cross(dir, Vector3.up).normalized * edgeOffset;
				Gizmos.DrawLine(child.position + offset, toNodePos + offset);
				Gizmos.color = nodeDirColor;
				Gizmos.DrawWireCube(toNodePos - (dir * dirCubeOffset) + offset, Vector3.one * dirCubeScale);
				Vector3 costLabelPos = ((child.position + offset * 2) + (toNodePos + offset * 2)) * 0.5f + (dir * costOffset);
				Handles.Label(costLabelPos, edge.BaseCost.ToString("0\n") + edge.Cost.ToString("0.00"), style_cost);
			}
		}
	}

	public void UpdateHandles()
	{

	}
}