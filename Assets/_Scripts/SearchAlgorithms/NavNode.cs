﻿using System.Collections.Generic;
using UnityEngine;

public class NavNode : MonoBehaviour
{
	[SerializeField] private List<NavNode> linkedNodes = new List<NavNode>();

	[SerializeField] private List<Edge> edges = new List<Edge>();
	public List<Edge> Edges => edges;
	public int Idx { get; set; }

	public void RemoveLink()
	{

	}
}

[System.Serializable]
public class Edge
{
	[SerializeField] private NavNode fromNode;
	public NavNode FromNode => fromNode;
	[SerializeField] private NavNode toNode;
	public NavNode ToNode => toNode;
	[SerializeField] private float baseCost;
	public float Distance => Vector3.Distance(FromNode.transform.position, ToNode.transform.position);
	public float Cost => baseCost + Distance;
	public float BaseCost => baseCost;

	public Edge(NavNode _fromNode, NavNode _toNode)
	{
		fromNode = _fromNode;
		toNode = _toNode;
	}
}