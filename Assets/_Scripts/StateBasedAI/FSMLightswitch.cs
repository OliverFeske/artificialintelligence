﻿using UnityEngine;

public class FSMLightswitch : MonoBehaviour
{
	public LightswitchState switchedOn;
	public LightswitchState switchedOff;
	public LightswitchState currentState;

	private void Awake()
	{
		switchedOn = new SwitchedOn(this);
		switchedOn.fSMLightswitch = this;
		switchedOff = new SwitchedOff(this);
		switchedOff.fSMLightswitch = this;
		currentState = switchedOff;
	}

	private void Update()
	{
		currentState.OnUpdate();
	}

	public void ChangeState(LightswitchState state)
	{
		currentState.OnStateExit();
		currentState = state;
		currentState.OnStateEnter();
	}
}

public abstract class LightswitchState
{
	public abstract FSMLightswitch fSMLightswitch { get; set; }
	public abstract void OnStateEnter();
	public abstract void OnStateExit();
	public abstract void OnUpdate();

	public LightswitchState(FSMLightswitch fsmLightSwitch)
	{
		fSMLightswitch = fsmLightSwitch;
	}
}

public class SwitchedOn : LightswitchState
{
	public override FSMLightswitch fSMLightswitch { get; set; }
	private float timer;

	public SwitchedOn(FSMLightswitch fsmLightswitch) : base(fsmLightswitch)
	{

	}

	public override void OnStateEnter()
	{
		Debug.Log("Light is on");
	}

	public override void OnStateExit()
	{
		timer = 0f;
	}

	public override void OnUpdate()
	{
		timer += Time.deltaTime;
		if (timer > 5f)
		{
			fSMLightswitch.ChangeState(fSMLightswitch.switchedOff);
		}
	}
}

public class SwitchedOff : LightswitchState
{
	private FSMLightswitch fsmLightswitch;
	public override FSMLightswitch fSMLightswitch
	{
		get { return fsmLightswitch; }
		set { fsmLightswitch = value; }
	}

	public SwitchedOff(FSMLightswitch fsmLightswitch) : base(fsmLightswitch)
	{

	}

	public override void OnStateEnter()
	{
		Debug.Log("Light is off");
	}

	public override void OnStateExit()
	{
	}

	public override void OnUpdate()
	{
		Debug.Log("off is called");
		if (Input.GetButtonDown("Jump"))
		{
			fSMLightswitch.ChangeState(fSMLightswitch.switchedOn);
		}
	}
}