﻿public abstract class AgentStates
{
	public abstract AgentUtilityAI agentAI { get; protected set; }
	public abstract AgentStatesCollection stateCollection { get; protected set; }
	public abstract void OnStateEnter();
	public abstract void OnStateExit();
	public abstract void OnUpdate();

	public AgentStates(AgentUtilityAI agent, AgentStatesCollection agentStatesCollection)
	{
		agentAI = agentAI;
		stateCollection = agentStatesCollection;
	}
}
