﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentStatesCollection : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        
    }
}

public class Patrol : AgentStates
{
	public override AgentUtilityAI agentAI { get; protected set; }
	public override AgentStatesCollection stateCollection { get; protected set; }

	public Patrol(AgentUtilityAI agentAI, AgentStatesCollection agentStatesCollection) : base(agentAI, agentStatesCollection)
	{

	}

	public override void OnStateEnter()
	{
		agentAI.CurrentWayPoint = agentAI.Wp.GetRandomWaypoint(agentAI.OldWaypointIdx, agentAI);
		agentAI.Agent.SetDestination(agentAI.CurrentWayPoint);
	}

	public override void OnStateExit()
	{
	}

	public override void OnUpdate()
	{
		Vector3 dist = agentAI.CurrentWayPoint - agentAI.transform.position;
		if (dist.sqrMagnitude < agentAI.ArrivedDist)
		{
			agentAI.CurrentWayPoint = agentAI.Wp.GetRandomWaypoint(agentAI.OldWaypointIdx, agentAI);
			agentAI.Agent.SetDestination(agentAI.CurrentWayPoint);
		}
	}
}

public class Encounter : AgentStates
{
	public override AgentUtilityAI agentAI { get; protected set; }
	public override AgentStatesCollection stateCollection { get; protected set; }

	public Encounter(AgentUtilityAI agentAI, AgentStatesCollection agentStatesCollection) : base(agentAI, agentStatesCollection)
	{

	}

	public override void OnStateEnter()
	{
	}

	public override void OnStateExit()
	{
	}

	public override void OnUpdate()
	{
	}
}

public class Attack : AgentStates
{
	public override AgentUtilityAI agentAI { get; protected set; }
	public override AgentStatesCollection stateCollection { get; protected set; }

	public Attack(AgentUtilityAI agentAI, AgentStatesCollection agentStatesCollection) : base(agentAI, agentStatesCollection)
	{

	}

	public override void OnStateEnter()
	{
	}

	public override void OnStateExit()
	{
	}

	public override void OnUpdate()
	{
	}
}

public class RunAway : AgentStates
{
	public override AgentUtilityAI agentAI { get; protected set; }
	public override AgentStatesCollection stateCollection { get; protected set; }

	public RunAway(AgentUtilityAI agentAI, AgentStatesCollection agentStatesCollection) : base(agentAI, agentStatesCollection)
	{

	}

	public override void OnStateEnter()
	{
	}

	public override void OnStateExit()
	{
	}

	public override void OnUpdate()
	{
	}
}