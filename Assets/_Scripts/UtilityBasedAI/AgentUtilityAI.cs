﻿using UnityEngine;
using UnityEngine.AI;

public class AgentUtilityAI : MonoBehaviour
{

	public AgentStates patrol;
	public AgentStates encounter;
	public AgentStates attack;
	public AgentStates runAway;
	public AgentStates currentState;

	[SerializeField] private float arrivedDist = 0.5f;
	public float ArrivedDist
	{
		get { return arrivedDist; }
		set { arrivedDist = value; }
	}
	private int oldWaypointIdx;
	public int OldWaypointIdx
	{
		get { return oldWaypointIdx; }
		set { oldWaypointIdx = value; }
	}
	private NavMeshAgent agent;
	public NavMeshAgent Agent
	{
		get { return agent; }
		set { agent = value; }
	}
	private Waypoints wp;
	public Waypoints Wp
	{
		get { return wp; }
		set { wp = value; }
	}
	private AgentStatesCollection asCol;
	public AgentStatesCollection ASCol
	{
		get { return asCol; }
		set { asCol = value; }
	}
	private Vector3 currentWayPoint;
	public Vector3 CurrentWayPoint
	{
		get { return currentWayPoint; }
		set { currentWayPoint = value; }
	}

	#region MonoBehaviour
	void Start()
	{
		agent = GetComponent<NavMeshAgent>();
		wp = GameObject.Find("Waypoints").GetComponent<Waypoints>();
		asCol = GameObject.Find("AgentStatesCollection").GetComponent<AgentStatesCollection>();
		oldWaypointIdx = 0;

		Debug.Log(Wp);

		patrol = new Patrol(this, asCol);
		encounter = new Encounter(this, asCol);
		attack = new Attack(this, asCol);
		runAway = new RunAway(this, asCol);

		currentState = patrol;
		currentState.OnStateEnter();

	}
	void Update()
	{
		currentState.OnUpdate();
	}
	#endregion

	#region StateFunctions
	public void ChangeState(AgentStates state)
	{
		currentState.OnStateExit();
		currentState = state;
		currentState.OnStateEnter();
	}
	#endregion
}